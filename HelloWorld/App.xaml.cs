﻿using System.Diagnostics;
using Xamarin.Forms;

namespace HelloWorld
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new HelloWorldPage();
        }

        protected override void OnStart()
        {
            Debug.WriteLine("OnStart was Called");
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            Debug.WriteLine("OnSleep was Called");
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            Debug.WriteLine("OnResume was Called");
            // Handle when your app resumes
        }
    }
}
